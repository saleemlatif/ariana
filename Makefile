requirements:
	pip install -r requirements.txt --exists-action w

migrate:
	python manage.py migrate --noinput

serve:
	python manage.py runserver 0.0.0.0:8000

dev-up: requirements migrate
	make serve
