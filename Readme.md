# Prerequisites
Make sure you have the following things installed on your system

1. Python 3
2. virtual environment with python 3 (This is preferred not a requirement.)

# Installation
1. Unzip the package, open a shell console and go the directory containing the `manage.py` file.
2. Activate the virtual environment if you have create one.
3. run `make dev-up`, this command will install python required package, 
run migrations and start the server at `0.0.0.0:8000`
4. Open a browser and enter `localhost:8000` and you should see react front end to use the app.  
