"""
Helper functions.
"""
import json
from django.conf import settings
from django.http.response import HttpResponseBadRequest

from functools import wraps


def get_questionaire():
    with open(settings.QUESTIONAIRE_PATH) as file:
        return json.load(file)


def get_questions(data: dict):
    """
    Returns a list of questions with possible answers.

    Return:
        (list): A list containing dict as child where each dict has key as question and a list of answers as value.
    """
    questions = []

    for key, value in data.items():
        questions.append({key: list(value.keys())})

    return questions


def get_sub_dict(sub_dict_path: list, dict_: dict):
    """
    Follow the path to sub dict inside a bigger dict and return the smaller one.

    Arguments:
        sub_dict_path (list): A list containing keys of the sub dicts inside _dict/
        dict_ (dict): Bigger dict
    Returns:
        (dict): smaller dict from the given dict_
    """
    sub_dict = dict_

    for key in sub_dict_path:
        sub_dict = sub_dict[key]

    return sub_dict


def get_current_state(sub_dict_path: list, dict_: dict):
    """
    According to the path get the current state of the front end app.

    Arguments:
        sub_dict_path (list): A list containing keys of the sub dicts inside _dict/
        dict_ (dict): Bigger dict
    Returns:
        (dict): smaller dict from the given dict_
    """
    state = []

    while sub_dict_path:
        question, answer, *sub_dict_path = sub_dict_path
        state.append({
            "question": question,
            "answers": list(dict_[question].keys()),
            "answer": answer
        })
        dict_ = dict_[question][answer]

    return state


def handle_request_error(*error: Exception):
    """
    Decorator to handle common errors and send default http codes.

    Arguments:
        error (Exception): Python Exception or tuple of exceptions that when raised constitute a malformed request.

    Returns:
         (Callable): Decorator that handles malformed requests.
    """

    def wrapper(func):
        @wraps(func)
        def handler(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except error as e:
                return HttpResponseBadRequest(repr(e))

        return handler
    return wrapper
