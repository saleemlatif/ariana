import sys
import logging

from django.http.response import HttpResponse
from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions

from web.helpers import get_questions, get_questionaire, get_sub_dict, get_current_state, handle_request_error

logging.basicConfig(filename='logs.log', format='%(message)s', level=logging.DEBUG)
logger = logging.getLogger(__name__)
handler = logging.StreamHandler(sys.stdout)
logger.addHandler(handler)


class QuestionaireView(APIView):
    """
    View to list all questions.
    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.AllowAny,)

    def __log_session(self):
        logger.info(" -> ".join(self.request.session.get('data', [])))

    def __clear_session(self):
        self.request.session['data'] = []

    @handle_request_error(IndexError, KeyError)
    def get(self, request):
        """
        Return a list of all questions.
        """
        questionaire = get_questionaire()
        sub_dict = get_sub_dict(
            sub_dict_path=request.session.get('data', []),
            dict_=questionaire
        )
        return Response({
           "questions": get_questions(sub_dict),
           "state": get_current_state(
               request.session.get('data', []),
               questionaire
           )
        })

    @handle_request_error(IndexError, KeyError)
    def post(self, request):
        data = [request.data['question'], request.data['answer']]
        request.session['data'] = request.session.get('data', []) + data

        sub_dict = get_sub_dict(
            sub_dict_path=request.session.get('data', []),
            dict_=get_questionaire()
        )
        if isinstance(sub_dict, str):
            self.__log_session()
            self.__clear_session()
            return Response({"questions": sub_dict})
        else:
            return Response({
                "questions": get_questions(sub_dict)
            })


def clear_session(request):
    request.session['data'] = []
    return HttpResponse()


def index(request):
    return render(request, 'index.html', {})
