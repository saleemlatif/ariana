
class App extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
		loading: true,
		answeredQuestions: []
		};

		this.handleSubmission = this.handleSubmission.bind(this);
		this.startOver = this.startOver.bind(this);
	}
	componentDidMount() {
		fetch("http://localhost:8000/questionaire/")
		.then(res => res.json())
		.then((result) => {
			this.setState({
				"question": Object.keys(result.questions[0])[0],
				"answers": Object.values(result.questions[0])[0],
				"answeredQuestions": result.state || [],
				"loading": false
			})
		},
		(error) => {console.log("An Error Occurred during request.")}
		)
	}

	render() {
		return (
			<div>
				{this.renderPreviousSubmissions()}

				{
					this.state.loading ?
					<div className="box button is-primary is-loading full-width"></div>:
					<Question
						question={this.state.question}
						answers={this.state.answers}
						onSubmit={this.handleSubmission}
					></Question>
				}
				<button className="button is-primary box" onClick={this.startOver}>start over</button>
			</div>
		)
	}

	renderPreviousSubmissions() {
		if(this.state.answeredQuestions)
			return this.state.answeredQuestions.map(
				question => (
					<Question
						question={question.question}
						answers={question.answers}
						answer={question.answer}
						onSubmit={() => {}}
						disabled={true}
					></Question>
				)
			)
	}

	handleSubmission (answer) {
		let answeredQuestions = this.state.answeredQuestions.concat([
			{
				"question": this.state.question,
				"answers": this.state.answers,
				"answer": answer
			}
		]);

		this.setState({
			"loading": true
		})

		this.postAnswer(
			this.state.question,
			answer,
			()=>{
				this.setState({
					"answeredQuestions": answeredQuestions,
					"loading": false
				})
			}
		);

	}

	postAnswer(question, answer, onSuccess) {
		fetch("http://localhost:8000/questionaire/", {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				question: question,
				answer: answer,
			})
		})
		.then(res => res.json())
		.then(res => res.questions)
		.then((result) => {
			onSuccess(result);

			if (typeof result == 'string') {
				this.setState({
					"question": result,
					"answers": []
				})
			}
			else {
				this.setState({
					"question": Object.keys(result[0])[0],
					"answers": Object.values(result[0])[0]
				})
			}
		},
		(error) => {
			alert("An Error Occurred during request. Make sure you are connected to the internet and try again.");
			this.setState({
				"loading": false
			})
		}
		)
	}

	startOver (e) {
		fetch("http://localhost:8000/clear-session/")
		.then(
			(result) => {
				location.reload();
			},
			(error) => {
				alert("An Error Occurred during request. Make sure you are connected to the internet and try again.");
			}
		)
	}

}

class Question extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
		  'disabled': this.props.disabled
		};

		this.optionSelected = this.optionSelected.bind(this);
	}

	render() {
		return <div className='box'>
			<span className='question'>{this.props.question}</span> {'\u00A0\u00A0\u00A0\u00A0'} {this.renderOptions()}
		</div>
	}


	renderOptions() {
		return this.props.answers.map(item => (
			<Option
				value={item}
				disabled={this.state.disabled}
				onSelect={this.optionSelected}
				selected={this.props.answer == item ? true:false }
			/>
		))
	}

	optionSelected (answer) {
		let question = this.props.question;

		this.setState({
			question: answer,
			disabled: true
		});

		this.props.onSubmit(answer);
	}

}

class Option extends React.Component {
	constructor (props) {
		super(props);
		this.state = {
			'selected': this.props.selected
		}
		this.handleClick = this.handleClick.bind(this);
	}

	render () {
		return <span
				className={'button options ' + (this.state.selected ? 'selected': '') }
				onClick={this.handleClick}
			>{this.props.value}</span>
	}

	handleClick(event) {
		if(!this.props.disabled) {
			this.setState({'selected': true});
			this.props.onSelect(this.props.value);
		}
	}
}

ReactDOM.render(
    <App/>,
    document.getElementById('render-target')
)
